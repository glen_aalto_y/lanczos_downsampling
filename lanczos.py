import torch.nn as nn
import numpy as np
import torch
import sys


class LanczosFilter(nn.Module):
    def _lanczos(self, x, a):
        """ Lanczos function applied to input x
        Args:
            x: input tensor
            a: half kernel size
        Return:
             computed tensor of the same size as x
        """
        eps = sys.float_info.epsilon
        return torch.sin(np.pi * x) / (np.pi * x + eps) * torch.sin(np.pi * x / a) / (np.pi * x / a + eps)

    def get_kernel(self, a):
        sample_num = 4 * a
        x = torch.linspace(-a + 0.25, a - 0.25, sample_num)
        y = self._lanczos(x, a)
        normed = y / torch.sum(y)
        c = normed.view(len(normed), 1)
        r = normed.view(1, len(normed))
        kernel = torch.mm(c, r)
        return kernel


class LanczosDownsampler(LanczosFilter):
    def __init__(self, in_channels, a, stride=2, padding_mode='zeros'):
        super(LanczosDownsampler, self).__init__()
        assert padding_mode in ['zeros', 'replicate']
        self.in_channels = in_channels
        self.stride = stride
        self.a = a
        self.padding_mode = padding_mode
        kernel = self.get_kernel(a)
        self.weights = kernel.expand(in_channels, 1, kernel.shape[0], kernel.shape[1]).float()

    def forward(self, x):
        if x.is_cuda and not self.weights.is_cuda:
            self.weights = self.weights.cuda()
        pad = [2*self.a-1] * 4
        if self.padding_mode == 'zeros':
            x = nn.functional.pad(x, pad, mode='constant', value=0)
        if self.padding_mode == 'replicate':
            x = nn.functional.pad(x, pad, mode='replicate')
        x = nn.functional.conv2d(x, self.weights, stride=self.stride, groups=self.in_channels)
        return x

